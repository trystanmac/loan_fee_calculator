# loan_fee_calculator

### Setup:

1. clone code
2. From the project root directory on the master branch:

``` 
$ composer install
$ docker-compose up -d
```

Get the container id:

```
$ docker ps
```

Run the unit tests:
```
$ docker exec -it <containerId> vendor/bin/phpunit
```

Run the unit tests:
```
$ docker exec -it <containerId> vendor/bin/phpunit
```

Run the user story tests:
```
$ docker exec -it <containerId> vendor/bin/behat
```