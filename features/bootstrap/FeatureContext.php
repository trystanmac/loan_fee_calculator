<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Lendable\Interview\Interpolation\Model\FeeInterpolator;
use Lendable\Interview\Interpolation\Model\FeeRounder;
use Lendable\Interview\Interpolation\Model\LinearGraph\CoordinateX;
use Lendable\Interview\Interpolation\Model\LinearGraph\Gradient;
use Lendable\Interview\Interpolation\Model\LinearGraph\Intercept;
use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /** @var FeeCalculator $feeCalculator */
    static $feeCalculator;

    /** @var LoanApplication $loanApplication */
    static $loanApplication;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I have loaded the data for my loans
     */
    public function iHaveLoadedTheDataForMyLoans()
    {
        $feeInterpolator = new FeeInterpolator(
            new Gradient,
            new Intercept(),
            new CoordinateX()
        );

        foreach (self::getTerm12Data() as $loan => $fee) {
            $feeInterpolator->addTermAmountFee(12, $loan, $fee);
        }

        foreach (self::getTerm24Data() as $loan => $fee) {
            $feeInterpolator->addTermAmountFee(24, $loan, $fee);
        }

        self::$feeCalculator = new FeeCalculator(
            $feeInterpolator,
            new FeeRounder()
        );
    }

    /**
     * @Given have created a :loanTerm loan for :loanAmount pounds
     */
    public function haveCreatedATermLoanFor($loanTerm, $loanAmount)
    {
        self::$loanApplication = new LoanApplication($loanTerm, $loanAmount);
    }

    /**
     * @Then I calculate the fee to be :feeAmount
     */
    public function iCalculateTheFeeToBe($feeAmount)
    {
        Assert::assertEquals(
            $feeAmount,
            self::$feeCalculator->calculate(self::$loanApplication)
        );
    }

    /**
     * Term 24 month data
     *
     * @return array
     */
    static private function getTerm24Data()
    {
        return [
            1000 => 70,
            2000 => 100,
            3000 => 120,
            4000 => 160,
            5000 => 200,
            6000 => 240,
            7000 => 280,
            8000 => 320,
            9000 => 360,
            10000 => 400,
            11000 => 440,
            12000 => 480,
            13000 => 520,
            14000 => 560,
            15000 => 600,
            16000 => 640,
            17000 => 680,
            18000 => 720,
            19000 => 760,
            20000 => 800,
        ];
    }

    /**
     * Term 12 month data
     *
     * @return array
     */
    static private function getTerm12Data()
    {
        return [
            1000 => 50,
            2000 => 90,
            3000 => 90,
            4000 => 115,
            5000 => 100,
            6000 => 120,
            7000 => 140,
            8000 => 160,
            9000 => 180,
            10000 => 200,
            11000 => 220,
            12000 => 240,
            13000 => 260,
            14000 => 280,
            15000 => 300,
            16000 => 320,
            17000 => 340,
            18000 => 360,
            19000 => 380,
            20000 => 400,
        ];
    }
}
