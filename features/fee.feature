Feature: Calculate fee for a loan

  Scenario Outline:: Calculate fee for a loan
    Given I have loaded the data for my loans
    And have created a <months> loan for <loanAmount> pounds
    Then I calculate the fee to be <fee>

  Examples:
    | months | loanAmount |   fee   |
    |  24   |    2750    |  115.0  |
    |  12   |    4800    |  105.0  |
    |  12   |    2750    |  90.0   |
