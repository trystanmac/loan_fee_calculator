<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model;

use Lendable\Interview\Interpolation\Model\LinearGraph\CoordinateX;
use Lendable\Interview\Interpolation\Model\LinearGraph\Gradient;
use Lendable\Interview\Interpolation\Model\LinearGraph\Intercept;

/**
 * Class FeeInterpolator
 * @package Lendable\Interview\Interpolation\Model
 */
class FeeInterpolator
{
    /** @var array $feeCalculatorData */
    private $feeCalculatorData = [];

    /** @var Gradient $gradient */
    private $gradient;

    /** @var Intercept $intercept */
    private $intercept;

    /** @var CoordinateX $coordinateX */
    private $coordinateX;

    /**
     * FeeInterpolator constructor.
     * @param Gradient $gradient
     * @param Intercept $intercept
     * @param CoordinateX $coordinateX
     */
    public function __construct(
        Gradient $gradient,
        Intercept $intercept,
        CoordinateX $coordinateX
    ) {
        $this->gradient = $gradient;
        $this->intercept = $intercept;
        $this->coordinateX = $coordinateX;
    }

    /**
     * @param int $term
     * @param int $amount
     * @param int $fee
     * @return $this
     */
    public function addTermAmountFee(int $term, int $amount, int $fee): self
    {
        $this->feeCalculatorData[$term][] = [
            'fee' => $fee,
            'amount' => $amount,
        ];

        return $this;
    }

    /**
     * @param int $term
     * @param float $amount
     * @return float
     * @throws \Exception
     */
    public function get(int $term, float $amount): float
    {
        /**
         * @todo Add exception handling if the following
         * values do not get set.
         */
        $lowerBoundFee = null;
        $lowerBoundAmount = null;

        $upperBoundFee = null;
        $upperBoundAmount = null;

        /**
         * @todo Add exception handling for:
         *  - $this->feeCalculatorData[$term] data not existing.
         */
        foreach ($this->feeCalculatorData[$term] as $key => $feeCalculatorData) {
            if ($feeCalculatorData['amount'] > $amount) {

                $lowerBoundFee =  $this->feeCalculatorData[$term][($key -1)]['fee'];
                $lowerBoundAmount =  $this->feeCalculatorData[$term][($key -1)]['amount'];

                $upperBoundFee =  $this->feeCalculatorData[$term][($key)]['fee'];
                $upperBoundAmount =  $this->feeCalculatorData[$term][($key)]['amount'];

                break;
            }
        }

        // If the fee is the same for the upper and lower fee,
        // there's no need to calculate further. The fee
        // can be returned as upper or lower.
        if ($lowerBoundFee == $upperBoundFee) {
            return $lowerBoundFee;
        }

        $this->gradient
            ->addCoordinates($lowerBoundFee, $lowerBoundAmount)
            ->addCoordinates($upperBoundFee, $upperBoundAmount);

        $gradient = $this->gradient->getGradient();

        $intercept = $this->intercept->get($lowerBoundFee, $lowerBoundAmount, $gradient);

        return $this->coordinateX->get($amount, $intercept, $gradient);
    }
}