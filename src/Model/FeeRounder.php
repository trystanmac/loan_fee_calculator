<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model;

/**
 * Class FeeRounder
 *
 * The business logic for this class is:
 *
 * The fee should then be "rounded up" such that (fee + loan amount)
 * is an exact multiple of 5.
 *
 * @package Lendable\Interview\Interpolation\Model
 */
class FeeRounder
{
    /**
     * @param float $amount
     * @param float $fee
     * @return float
     */
    public function get(float $amount, float  $fee): float
    {
        /**
         * This could be set in the constructor if flexibility was
         * required here.
         */
        $multiple = 5;

        $combined = $amount + $fee;

        $roundUp = 0;

        $mod = ($combined % $multiple);

        if (0 !== $mod) {
            $roundUp = $multiple - ($combined % $multiple);
        }

        return (float) $fee + $roundUp;
    }
}