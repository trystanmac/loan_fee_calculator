<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model\LinearGraph;

/**
 * Class CoordinateX
 * @package Lendable\Interview\Interpolation\Model\LinearGraph
 */
class CoordinateX
{
    /**
     * @param float $y
     * @param float $c
     * @param float $m
     * @return float
     */
    public function get(float $y, float $c, float $m): float
    {
        return ($y - $c) / $m;
    }
}