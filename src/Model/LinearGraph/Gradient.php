<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model\LinearGraph;

use Exception;

/**
 * Class Gradient
 * @package Lendable\Interview\Interpolation\Model\LinearGraph
 */
class Gradient
{
    private $coordinates;

    /**
     * @param int $x
     * @param int $y
     * @return Gradient
     */
    public function addCoordinates(int $x, int $y): self
    {
        $this->coordinates[] = [
            'x' => $x,
            'y' => $y,
        ];

        return $this;
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function getGradient(): float
    {
        if (count($this->coordinates) != 2) {
            throw new Exception('Error, two sets of x and y coordinates required.');
        }

        $lowerSet = array_shift($this->coordinates);
        $upperSet = array_pop($this->coordinates);

        /**
         * @todo Handle the situation where ($upperSet['x'] - $lowerSet['x']) == 0
         * i.e. gradient = 1
         */
        return (float) ($upperSet['y'] - $lowerSet['y']) / ($upperSet['x'] - $lowerSet['x']);
    }
}