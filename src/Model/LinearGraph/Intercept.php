<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Model\LinearGraph;


/**
 * Class Intercept
 * @package Lendable\Interview\Interpolation\Model\LinearGraph
 */
class Intercept
{
    /**
     * @param float $x
     * @param float $y
     * @param float $m
     * @return float
     */
    public function get(float $x, float $y, float $m): float
    {
        return ($y - ($m * $x));
    }
}