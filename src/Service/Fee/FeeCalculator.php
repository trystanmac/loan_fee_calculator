<?php

declare(strict_types=1);

namespace Lendable\Interview\Interpolation\Service\Fee;

use Exception;
use Lendable\Interview\Interpolation\Model\FeeInterpolator;
use Lendable\Interview\Interpolation\Model\FeeRounder;
use Lendable\Interview\Interpolation\Model\LoanApplication;

/**
 * Class FeeCalculator
 * @package Lendable\Interview\Interpolation\Service\Fee
 */
class FeeCalculator implements FeeCalculatorInterface
{
    /**
     * @todo consider these to be configurable in the future so that the values
     * can be injected.
     */
    const LOAN_AMOUNT_MINIMUM = 1000;
    const LOAN_AMOUNT_MAXIMUM = 20000;

    /** @var FeeInterpolator $feeInterpolator */
    private $feeInterpolator;

    /** @var FeeRounder $feeRounder */
    private $feeRounder;

    public function __construct(
        FeeInterpolator $feeInterpolator,
        FeeRounder $feeRounder
    ) {
        $this->feeInterpolator = $feeInterpolator;
        $this->feeRounder = $feeRounder;
    }

    /**
     * @param LoanApplication $application
     * @return float
     * @throws Exception
     */
    public function calculate(LoanApplication $application): float
    {
        $applicationAmount = $application->getAmount();

        if ($applicationAmount < self::LOAN_AMOUNT_MINIMUM || $applicationAmount > self::LOAN_AMOUNT_MAXIMUM) {
            throw new Exception(
                sprintf(
                    'Loan amounts should be between £%s and £%s',
                    self::LOAN_AMOUNT_MINIMUM,
                    self::LOAN_AMOUNT_MAXIMUM
                )
            );
        }

        $applicationTerm = $application->getTerm();

        $interpolatedFee = $this->feeInterpolator->get($applicationTerm, $applicationAmount);

        return $this->feeRounder->get($applicationAmount, $interpolatedFee);
    }
}