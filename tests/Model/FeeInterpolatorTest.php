<?php


namespace LendableTest\Interview\Interpolation\Model;

use Lendable\Interview\Interpolation\Model\FeeInterpolator;
use Lendable\Interview\Interpolation\Model\FeeRounder;
use Lendable\Interview\Interpolation\Model\LinearGraph\CoordinateX;
use Lendable\Interview\Interpolation\Model\LinearGraph\Gradient;
use Lendable\Interview\Interpolation\Model\LinearGraph\Intercept;
use Lendable\Interview\Interpolation\Model\LoanApplication;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class FeeCalculatorTest
 * @package LendableTest\Interview\Interpolation\Service\Fee
 */
class FeeInterpolatorTest extends TestCase
{
    /** @var FeeInterpolator $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | Gradient $linearGraphGradientMock */
    private $linearGraphGradientMock;

    /** @var MockObject | Intercept $interceptMock */
    private $interceptMock;

    /** @var MockObject | CoordinateX $coordinateXMock */
    private $coordinateXMock;

    public function setUp(): void
    {
        $this->linearGraphGradientMock = $this->createMock(Gradient::class);
        $this->interceptMock = $this->createMock(Intercept::class);
        $this->coordinateXMock = $this->createMock(CoordinateX::class);

        $this->objectUnderTest = new FeeInterpolator(
            $this->linearGraphGradientMock,
            $this->interceptMock,
            $this->coordinateXMock
        );
    }

    public function testAddTermAmountFeeFluidInterface(): void
    {
        $term = rand(10, 19);
        $amount = rand(20, 29);
        $fee = rand(30, 39);

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->addTermAmountFee($term, $amount, $fee)
        );
    }

    public function testGet(): void
    {
        $term = 24;
        $amount = (float) 2750;

        $this->objectUnderTest
            ->addTermAmountFee(24, 1000, 70)
            ->addTermAmountFee(24, 2000, 100)
            ->addTermAmountFee(24, 3000, 120);

        $this->linearGraphGradientMock
            ->expects($this->at(0))
            ->method('addCoordinates')
            ->with(100, 2000)
            ->willReturnSelf();

        $this->linearGraphGradientMock
            ->expects($this->at(1))
            ->method('addCoordinates')
            ->with(120, 3000);

        $gradient = (float) rand(100, 199);
        $this->linearGraphGradientMock
            ->expects($this->once())
            ->method('getGradient')
            ->willReturn($gradient);

        $intercept = (float) rand(200, 299);
        $this->interceptMock
            ->expects($this->once())
            ->method('get')
            ->with(100, 2000, $gradient)
            ->willReturn($intercept);

        $xCoordinate = (float) rand(300, 399);
        $this->coordinateXMock
            ->expects($this->once())
            ->method('get')
            ->with($amount, $intercept, $gradient)
            ->willReturn($xCoordinate);

        $this->assertEquals(
            $xCoordinate,
            $this->objectUnderTest->get($term, $amount)
        );
    }

    public function testGetWhereConsecutiveLoanAmountHasSameFee(): void
    {
        $term = 24;
        $amount = (float) 2750;

        $this->objectUnderTest
            ->addTermAmountFee(24, 1000, 70)
            ->addTermAmountFee(24, 2000, 115)
            ->addTermAmountFee(24, 3000, 115);

        $this->linearGraphGradientMock
            ->expects($this->never())
            ->method('addCoordinates');

        $this->linearGraphGradientMock
            ->expects($this->never())
            ->method('getGradient');

        $this->interceptMock
            ->expects($this->never())
            ->method('get');

        $this->coordinateXMock
            ->expects($this->never())
            ->method('get');

        $this->assertEquals(
            115,
            $this->objectUnderTest->get($term, $amount)
        );
    }
}