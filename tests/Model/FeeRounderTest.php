<?php

declare(strict_types=1);

namespace LendableTest\Interview\Interpolation\Model;

use Lendable\Interview\Interpolation\Model\FeeRounder;
use PHPUnit\Framework\TestCase;

/**
 * Class FeeRounderTest
 * @package LendableTest\Interview\Interpolation\Model
 */
class FeeRounderTest extends TestCase
{
    /**
     * @var FeeRounder
     */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new FeeRounder();
    }

    public function testGet(): void
    {
        $amount = 17;
        $fee = 2;

        $expectedRoundedFee = $fee + 1;

        $this->assertEquals(
            $expectedRoundedFee,
            $this->objectUnderTest->get($amount, $fee)
        );
    }

    public function testGet2(): void
    {
        $amount = 21;
        $fee = 6;

        $expectedRoundedFee = $fee + 3;

        $this->assertEquals(
            $expectedRoundedFee,
            $this->objectUnderTest->get($amount, $fee)
        );
    }

    public function testGetWithFloat(): void
    {
        $amount = 21;
        $fee = 5.5;

        $expectedRoundedFee = $fee + 4;

        $this->assertEquals(
            $expectedRoundedFee,
            $this->objectUnderTest->get($amount, $fee)
        );
    }

    public function testGetNoRoundUp(): void
    {
        $amount = 17;
        $fee = 3;

        $this->assertEquals(
            $fee,
            $this->objectUnderTest->get($amount, $fee)
        );
    }
}