<?php

declare(strict_types=1);

namespace LendableTest\Interview\Interpolation\Model\LinearGraph;

use Lendable\Interview\Interpolation\Model\LinearGraph\CoordinateX;
use PHPUnit\Framework\TestCase;

/**
 * Class CoordinateXTest
 * @package LendableTest\Interview\Interpolation\Model\LinearGraph
 */
class CoordinateXTest extends TestCase
{
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new CoordinateX;
    }

    public function testGet(): void
    {
        $y = 10;
        $c = 3;
        $m = 2;

        $expectedX = 3.5;

        $this->assertEquals(
            $expectedX,
            $this->objectUnderTest->get($y, $c, $m)
        );
    }

    public function testGetRandom(): void
    {
        $y = rand(10, 20);
        $c = rand(20, 30);
        $m = rand(30, 40);

        $expectedX = ($y - $c) / $m;

        $this->assertEquals(
            $expectedX,
            $this->objectUnderTest->get($y, $c, $m)
        );
    }
}