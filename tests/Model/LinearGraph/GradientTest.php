<?php

declare(strict_types=1);

namespace LendableTest\Interview\Interpolation\Service\Fee;

use Exception;
use Lendable\Interview\Interpolation\Model\LinearGraph\Gradient;
use PHPUnit\Framework\TestCase;

/**
 * Class GradientTest
 * @package LendableTest\Interview\Interpolation\Service\Fee
 */
class GradientTest extends TestCase
{
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new Gradient();
    }

    public function testAddCoordinates(): void
    {
        $x1 = 100;
        $y1 = 1000;

        $this->assertSame(
            $this->objectUnderTest,
            $this->objectUnderTest->addCoordinates($x1, $y1)
        );
    }

    public function testAddCoordinatesAndGetGradient(): void
    {
        $this->objectUnderTest
            ->addCoordinates(100, 2000)
            ->addCoordinates(120, 3000);

        $this->assertEquals(
            50,
            $this->objectUnderTest->getGradient()
        );
    }

    public function testAddCoordinatesAndGetGradientGenralisedTest(): void
    {
        $x1 = rand(100, 199);
        $y1 = rand(100, 199);

        $x2 = rand(200, 299);
        $y2 = rand(200, 299);

        $m = ($y2 - $y1) / ($x2 - $x1);

        $this->objectUnderTest
            ->addCoordinates($x1, $y1)
            ->addCoordinates($x2, $y2);

        $this->assertEquals(
            $m,
            $this->objectUnderTest->getGradient()
        );
    }

    public function testAddTooManyCoordinatesAndGetGradient(): void
    {
        $this->objectUnderTest
            ->addCoordinates(100, 2000)
            ->addCoordinates(120, 3000)
            ->addCoordinates(125, 4000);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, two sets of x and y coordinates required.');

        $this->objectUnderTest->getGradient();
    }

    public function testAddTooFewCoordinatesAndGetGradient()
    {
        $this->objectUnderTest
            ->addCoordinates(100, 2000);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Error, two sets of x and y coordinates required.');

        $this->objectUnderTest->getGradient();
    }
}