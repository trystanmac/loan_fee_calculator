<?php

declare(strict_types=1);

namespace LendableTest\Interview\Interpolation\Model\LinearGraph;

use Lendable\Interview\Interpolation\Model\LinearGraph\Intercept;
use PHPUnit\Framework\TestCase;

class InterceptTest extends TestCase
{
    /**
     * @var Intercept
     */
    private $objectUnderTest;

    public function setUp(): void
    {
        $this->objectUnderTest = new Intercept();
    }

    public function testGet(): void
    {
        $x = 5;
        $y = 4;
        $m = 2;

        $expectedIntercept = $y - ($m * $x);

        $this->assertEquals(
            $expectedIntercept,
            $this->objectUnderTest->get($x, $y, $m)
        );
    }

    public function testGetWithRandomNums(): void
    {
        $x = rand(10, 20);
        $y = rand(10, 30);
        $m = rand(1, 9);

        $expectedIntercept = $y - ($m * $x);

        $this->assertEquals(
            $expectedIntercept,
            $this->objectUnderTest->get($x, $y, $m)
        );
    }
}