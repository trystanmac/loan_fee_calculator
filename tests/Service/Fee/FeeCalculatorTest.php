<?php


namespace LendableTest\Interview\Interpolation\Service\Fee;

use Exception;
use Lendable\Interview\Interpolation\Model\FeeInterpolator;
use Lendable\Interview\Interpolation\Model\FeeRounder;
use Lendable\Interview\Interpolation\Model\LoanApplication;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculator;
use Lendable\Interview\Interpolation\Service\Fee\FeeCalculatorInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class FeeCalculatorTest
 * @package LendableTest\Interview\Interpolation\Service\Fee
 */
class FeeCalculatorTest extends TestCase
{
    /** @var FeeCalculator $objectUnderTest */
    private $objectUnderTest;

    /** @var MockObject | FeeInterpolator $coordinateXMock */
    private $feeInterpolatorMock;

    /** @var MockObject | FeeRounder $coordinateXMock */
    private $feeRounderMock;

    public function setUp(): void
    {
        $this->feeInterpolatorMock = $this->createMock(FeeInterpolator::class);
        $this->feeRounderMock = $this->createMock(FeeRounder::class);

        $this->objectUnderTest = new FeeCalculator(
            $this->feeInterpolatorMock,
            $this->feeRounderMock
        );
    }

    public function testImplementsFeeCalculatorInterface(): void
    {
        $this->assertInstanceOf(
            FeeCalculatorInterface::class,
            $this->objectUnderTest
        );
    }

    public function testCalculate(): void
    {
        $loanApplicationMock = $this->createMock(LoanApplication::class);

        $term = 24;
        $loanApplicationMock
            ->expects($this->once())
            ->method('getTerm')
            ->willReturn($term);

        $amount = (float) 2750;
        $loanApplicationMock
            ->expects($this->once())
            ->method('getAmount')
            ->willReturn($amount);

        $interpolatedFee = (float) rand(500, 599);
        $this->feeInterpolatorMock
            ->expects($this->once())
            ->method('get')
            ->with($term, $amount)
            ->willReturn($interpolatedFee);

        $feeRounded = (float) rand(400, 499);
        $this->feeRounderMock
            ->expects($this->once())
            ->method('get')
            ->with($amount, $interpolatedFee)
            ->willReturn($feeRounded);

        $this->assertEquals(
            $feeRounded,
            $this->objectUnderTest->calculate($loanApplicationMock)
        );
    }

    public function testCalculateLoanLessThan1000(): void
    {
        $loanApplicationMock = $this->createMock(LoanApplication::class);

        $amount = (float) 999;
        $loanApplicationMock
            ->expects($this->once())
            ->method('getAmount')
            ->willReturn($amount);

        $loanApplicationMock
            ->expects($this->never())
            ->method('getTerm');

        $this->feeInterpolatorMock
            ->expects($this->never())
            ->method('get');

        $this->feeRounderMock
            ->expects($this->never())
            ->method('get');

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Loan amounts should be between £1000 and £20000');

        $this->objectUnderTest->calculate($loanApplicationMock);
    }

    public function testCalculateLoanLessGreaterThan20000(): void
    {
        $loanApplicationMock = $this->createMock(LoanApplication::class);

        $amount = (float) 20001;
        $loanApplicationMock
            ->expects($this->once())
            ->method('getAmount')
            ->willReturn($amount);

        $loanApplicationMock
            ->expects($this->never())
            ->method('getTerm');

        $this->feeInterpolatorMock
            ->expects($this->never())
            ->method('get');

        $this->feeRounderMock
            ->expects($this->never())
            ->method('get');

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Loan amounts should be between £1000 and £20000');

        $this->objectUnderTest->calculate($loanApplicationMock);
    }
}